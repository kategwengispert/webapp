from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.base2, name='base2'),
    url(r'^post/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),
]
